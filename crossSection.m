function [TVals]=crossSection(n,diffuseFunc,hs,endpts,plotsOn)
%crossSection(n, diffuseFunc, hs, endpts, plotsOn): creates plot of temp
%distribution 
    %INPUTS: 
         %n: size of array/grid
        %diffuseFunc: the function from diffusefunctions.m
        %hs: 3 column array, #rows = heat source locations, column 1 = a,
        %column 2 = x location of heat sources column 3 = y locations of
        %heat sources
        %plotsOn: if 'on', plots a contour map with heat source locations
        %endpts: 2x2 array of endpoints of x and y 
%OUTPUT:
    %TVals: n by n matrix made from linear combinations of diffuse function

%store endpoints into x1, x2, y1, y2 values 
x1 = endpts(1, 1);
y1 = endpts(1, 2);
x2 = endpts(2, 1);
y2 = endpts(2, 2); 


%preallocate TVals
TVals = zeros(1, n); 

%set a=first column, u = last 2 columns
a = hs(:,1);
u = hs(:, 2: 3);
%kMax = rows
[kMax, c] = size(hs); 
%x and y with appropriate endponts 
x = linspace(x1, x2, n);  
y = linspace(y1, y2, n);
%get x axis 
d = linspace(0, 1, n); 
 %use the k for loop to add to TVALs with a summation , using diffuseFunc
for k=1: kMax
    TVals = TVals + (a(k)  * diffuseFunc(x, y, u(k,:)));  %Kth row cols 1 and 2 
    
end


%if plots on is on, plot the cross section 
if strcmp(plotsOn, 'on')
   
 
w = func2str(diffuseFunc);

plot(d, TVals); %d is x, tvals is y 
title(sprintf('Cross section of Temperature Grid for %d heat source locations\n Using %s Diffusion Equation', kMax, w));    
xlabel('x');
ylabel('f(x)'); 



%add legend 

end


end
