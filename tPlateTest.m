%% load diffuseFunciotns
diffuseFunctions

%%
disp('tPlate1Test');
u=[1.5,1.5];
x=linspace(0,3,40);y=linspace(0,3,40);[X,Y]=meshgrid(x,y);
tVals = tPlate1(X,Y,u);
contour(tVals);
axis 'equal'
tVals1=tVals(10:15,20:25);
tVals1_cor=[ 0.5200    0.5200    0.5139    0.5019    0.4844    0.4620
    0.5854    0.5854    0.5785    0.5650    0.5453    0.5200
    0.6512    0.6512    0.6435    0.6285    0.6065    0.5785
    0.7158    0.7158    0.7074    0.6909    0.6668    0.6359
    0.7776    0.7776    0.7685    0.7505    0.7243    0.6909
    0.8349    0.8349    0.8251    0.8058    0.7776    0.7417];
tol1=1e-4;
assert( max(abs(tVals1(:)-tVals1_cor(:)))< tol1 , ...
    [ '\nYour output \ntVals1 = [ \n' sprintf('  %5.3f  %5.3f %5.3f %5.3f %5.3f %5.3f \n',tVals1') ']\n\n' ...                     
      'Expected output \ntVals1_cor = [ \n' sprintf(' %5.3f %5.3f %5.3f %5.3f %5.3f %5.3f \n',tVals1_cor') ']\n'], ...
      tVals1,tVals1_cor);
%%
disp('tPlate2Test');
u=[1.5,1.5];
x=linspace(0,3,40);y=linspace(0,3,40);[X,Y]=meshgrid(x,y);
tVals = tPlate2(X,Y,u);
contour(tVals);
axis 'equal'
tVals1=tVals(10:15,20:25);
tVals1_cor=[ 0.2711    0.2711    0.2698    0.2673    0.2635    0.2586
    0.3435    0.3435    0.3419    0.3386    0.3339    0.3276
    0.4250    0.4250    0.4230    0.4190    0.4131    0.4054
    0.5136    0.5136    0.5112    0.5064    0.4992    0.4899
    0.6062    0.6062    0.6033    0.5976    0.5892    0.5781
    0.6987    0.6987    0.6954    0.6888    0.6791    0.6664];
tol1=1e-4;
assert( max(abs(tVals1(:)-tVals1_cor(:)))< tol1 , ...
    [ '\nYour output \ntVals1 = [ \n' sprintf('  %5.3f  %5.3f %5.3f %5.3f %5.3f %5.3f \n',tVals1) ']\n\n' ...                     
      'Expected output \ntVals1_cor = [ \n' sprintf(' %5.3f %5.3f %5.3f %5.3f %5.3f %5.3f \n',tVals1_cor) ']\n'], ...
      tVals1,tVals1_cor);
%%
disp('tPlate3Test');
u=[1.5,1.5];
x=linspace(0,3,40);y=linspace(0,3,40);[X,Y]=meshgrid(x,y);
tVals = tPlate3(X,Y,u);
contour(tVals);
axis 'equal'
tVals1=tVals(10:15,20:25);
tVals1_cor=[ 0.4951    0.4951    0.4609    0.3994    0.3222    0.2420
    0.5713    0.5713    0.5318    0.4609    0.3718    0.2792
    0.6499    0.6499    0.6050    0.5243    0.4229    0.3176
    0.7288    0.7288    0.6784    0.5879    0.4743    0.3562
    0.8056    0.8056    0.7500    0.6499    0.5243    0.3937
    0.8779    0.8779    0.8172    0.7082    0.5713    0.4290];
tol1=1e-4;
assert( max(abs(tVals1(:)-tVals1_cor(:)))< tol1 , ...
    [ '\nYour output \ntVals1 = [ \n' sprintf('  %5.3f  %5.3f %5.3f %5.3f %5.3f %5.3f \n',tVals1) ']\n\n' ...                     
      'Expected output \ntVals1_cor = [ \n' sprintf(' %5.3f %5.3f %5.3f %5.3f %5.3f %5.3f \n',tVals1_cor) ']\n'], ...
      tVals1,tVals1_cor);