function [arrayOut] = arrayBoundary(arrayIn, n)
%arrayBoundary(arrayIn,n):creates an n sized boundary around an array that
%contains elements refelcted from within the array
%INPUTS
    %arrayIn: the initial array that will have a reflecting boundary added 
    %n: the amount of boundary width to be added
%OUTPUTS
    %arrayOut: a new array with arrayIn in the center, and with a boundary
    %of size n that contains elements reflected from arrayIn 
    
% Get arrayIn dimensions
[r, c] = size(arrayIn); 

%Use formula to calculate the new row and column size of arrayOut
newRows = r + (2 * n);
newCols = c + (2 * n);


%preallocate arrayOut

arrayOut=zeros(newRows,newCols); 

%place arrayIn in the cneter of arrayOut
%1+n is one past the boundary. n + r is boundary + number of rows, n + c is
%the boundary + the number of columns
arrayOut(1+n: n+r ,n+1:n+c)=arrayIn;


 %left column 
 %Replace arrayOut boundary (the first column to the n column)
 %get the reflected area by going backwards from n+1+n (the rightmost part
 %of the column in arrayIn to grab by going over n, to n+1+1 the first column of arrayIn
arrayOut(:,1:n) = arrayOut(:,n+1+n:-1:n+1+1);

%right column
%This is like the left column. newCols is analgous to 1, the outermost part
%of arrayIn to be changed. n+1+c goes to the other part of the boundary,
%similiar to n on the left column. 
%Grabs newColumns-n-1 and newCols-n-n
arrayOut(:,n+1+c:newCols) = arrayOut(:,newCols-n-1:-1:newCols-n-n); 

%top row: mimicks left column, though adjusted for rows
arrayOut(1:n,:) = arrayOut(n+1+n:-1:n+1+1,:);

%bottom row: mimicks right column, though adjusted for rows
arrayOut(n+1+r:newRows,:) = arrayOut(newRows-n-1:-1:newRows-n-n,:); 

end


