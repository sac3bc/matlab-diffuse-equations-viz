function [ arrayOut ] = arrayBoundary(arrayIn, n)
% Insert specification for function with inputs and outputs defined

% Get arrayIn dimensions

% Preallocate arrayOut and copy arrayIn into center region
% of preallocated array with boundary of width n using array-based
% opertions

arrayOut( )=arrayIn

% Use matrix indexing to specify blocks of arrayIn to copy to the 
% boundaries of arrayOut

% copy block of columns containing arrayIn reflection 
% about left border to block of columns containing left flap 
arrayOut( )=arrayOut();
% copy block of columns containing arrayIn reflection 
% about right border to block of columns containing right flap 
arrayOut( )=arrayOut( ); 
% copy block of rows containing arrayIn reflection 
% about top border to block of rows containing top flap 
arrayOut( )=arrayOut( ); 
% copy block of rows containing arrayIn reflection 
% about bottom border to block of rows containing bottom flap
arrayOut( )=arrayOut( ); 

end


