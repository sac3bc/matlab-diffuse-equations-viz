%% load diffuseFunctions
diffuseFunctions
%%
disp('crossSectionTest1')
endpts=[-2,-2;3,1];
nLoc=5;
hs=zeros(nLoc,3);
rng(1234)
hs(:,1)=randi(nLoc,nLoc,1);
hs(:,2)=6*rand(nLoc,1)-3;
hs(:,3)=6*rand(nLoc,1)-3;
n=200;
plotsOn='on';
[TVals]=crossSection(n,tPlate1,hs,endpts,plotsOn);
outputT=TVals(1:40:200);
outputT_cor= ...
       [   0.2255    1.1447    0.5195    0.9481    3.8189 ];
tol1=1e-3;
assert( max(max(abs(outputT-outputT_cor) ) ) < tol1 , ...
    [ '\nYour output [y] = [\n' sprintf('%8.3f  %8.3f %8.3f %8.3f  %8.3f \n',outputT') ']\n' ...
      '\nExpected output [y] = [\n' sprintf('%8.3f  %8.3f %8.3f %8.3f  %8.3f \n',outputT_cor') ']\n\n' ], ...
     outputT,outputT_cor);
%%
disp('crossSectionTest2')
endpts=[1,-2;2,2];
nLoc=4;
hs=zeros(nLoc,3);
rng(1235)
hs(:,1)=randi(nLoc,nLoc,1);
hs(:,2)=6*rand(nLoc,1)-3;
hs(:,3)=6*rand(nLoc,1)-3;
n=200;
plotsOn='on';
[TVals]=crossSection(n,tPlate2,hs,endpts,plotsOn);
outputT=TVals(1:40:200);
outputT_cor= ...
       [      0.174     2.681    3.115    2.497     3.478 ];
tol1=1e-3;
assert( max(max(abs(outputT-outputT_cor) ) ) < tol1 , ...
    [ '\nYour output [y] = [\n' sprintf('%8.3f  %8.3f %8.3f %8.3f  %8.3f \n',outputT') ']\n' ...
      '\nExpected output [y] = [\n' sprintf('%8.3f  %8.3f %8.3f %8.3f  %8.3f \n',outputT_cor') ']\n\n' ], ...
     outputT,outputT_cor);
%%
disp('crossSectionTest3')
endpts=[2,-3;-2,2];
nLoc=6;
hs=zeros(nLoc,3);
rng(1245)
hs(:,1)=randi(nLoc,nLoc,1);
hs(:,2)=6*rand(nLoc,1)-3;
hs(:,3)=6*rand(nLoc,1)-3;
n=200;
plotsOn='on';
[TVals]=crossSection(n,tPlate3,hs,endpts,plotsOn);
outputT=TVals(1:40:200);
outputT_cor= ...
       [    1.939     0.270    3.151    2.417     0.870 ];
tol1=1e-3;
assert( max(max(abs(outputT-outputT_cor) ) ) < tol1 , ...
    [ '\nYour output [y] = [\n' sprintf('%8.3f  %8.3f %8.3f %8.3f  %8.3f \n',outputT') ']\n' ...
      '\nExpected output [y] = [\n' sprintf('%8.3f  %8.3f %8.3f %8.3f  %8.3f \n',outputT_cor') ']\n\n' ], ...
     outputT,outputT_cor);


%%
disp('Performance Timing Test')
endpts=[-3,-3;3,3];
nLoc=10;
hs=zeros(nLoc,3);
rng(1234)
hs(:,1)=randi(nLoc,nLoc,1);
hs(:,2)=6*rand(nLoc,1)-3;
hs(:,3)=6*rand(nLoc,1)-3;
n=5000000;
plotsOn='off';

tic;
[TVals]=crossSection(n,tPlate3,hs,endpts,plotsOn);
time_student=toc

tic;
[TVals]=crossSectionRef(n,tPlate3,hs,endpts,plotsOn);
time_solution=toc

assert( (time_student/time_solution) < 1.2 , ...
    [ '\nYour timing \n P = [' sprintf(' %8.4f ',time_student) ']\n'   ...
      'Solution timing \n P = [' sprintf(' %8.4f ',time_solution) ']\n'   ], ...
      time_student,time_solution);