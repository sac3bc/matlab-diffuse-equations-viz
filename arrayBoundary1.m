function [arrayOut] = arrayBoundary1(arrayIn, n)
%arrayBoundary1(arrayIn, n):Creates an array with an n sized boundary with
%elements that are reflected from within the array using pad array and
%deleting redundant rows/columns
    %INPUTS
        %arrayIn: the initial array to be padded
        %n: the amount of padding to be added to the boundary of the array
     %OUTPUTS
        %arrayOut: the new array with boundary width n that is supposed to
   
  
%Use padarray to create symmetric padding 
arrayOut = padarray(arrayIn, [n+1 n+1], 'symmetric'); 
%get the number of rows and columns
[r,c] = size(arrayOut);
%delete the redundant right boudary column
arrayOut(:,c-n) = [];
%delete the redundant left boudnary column 
arrayOut(:,n+1) = [];
%delete redundant bottom boundary column
arrayOut(r-n,:) = [];
%delete redundant top boundary column  
arrayOut(n+1,:) = [];


end