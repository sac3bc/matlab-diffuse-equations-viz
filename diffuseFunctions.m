%diffuseFunctions.m
%script that contains the function definitions of 2D exponential functions with anonymous functions
a = 1.1; 
tPlate1 = @(x, y, u) exp((-1.0.*( ((x-u(1)).^2) + ((y-u(2)).^2)) )); 
tPlate2 = @(x, y, u) exp((-2.0.*(0.2.*((x-u(1)).^2) + ((y-u(2)).^2)) ));
tPlate3 = @(x, y, u) a * exp((-a.^(2).*((5.*((x-u(1)).^2) + ((y-u(2)).^2)) )));