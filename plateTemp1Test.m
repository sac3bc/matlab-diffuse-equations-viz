%% load diffuseFunctions
diffuseFunctions
%%
disp('plateTemp1Test1')
nLoc=5;
hs=zeros(nLoc,3);
rng(1234)
hs(:,1)=randi(nLoc,nLoc,1);
hs(:,2)=6*rand(nLoc,1)-3;
hs(:,3)=6*rand(nLoc,1)-3;
n=200;
plotsOn='on';
[TVals]=plateTemp1(n,tPlate1,hs,plotsOn);
outputT=(TVals(98:102,98:102));
outputT_cor= ...
       [      0.936     0.873    0.814    0.759     0.708
   0.936     0.873    0.814    0.759     0.709
   0.934     0.871    0.813    0.759     0.708
   0.931     0.869    0.811    0.757     0.707
   0.926     0.865    0.807    0.754     0.705];
tol1=2e-3;
assert( max(max(abs(outputT-outputT_cor) ) ) < tol1 , ...
    [ '\nYour output [y] = [\n' sprintf('%8.3f  %8.3f %8.3f %8.3f  %8.3f \n',outputT') ']\n' ...
      '\nExpected output [y] = [\n' sprintf('%8.3f  %8.3f %8.3f %8.3f  %8.3f \n',outputT_cor') ']\n\n' ], ...
     outputT,outputT_cor);
%%
disp('plateTemp1Test2')
nLoc=4;
hs=zeros(nLoc,3);
rng(1235)
hs(:,1)=randi(nLoc,nLoc,1);
hs(:,2)=6*rand(nLoc,1)-3;
hs(:,3)=6*rand(nLoc,1)-3;
n=200;
plotsOn='on';
[TVals]=plateTemp1(n,tPlate2,hs,plotsOn);
outputT=(TVals(98:102,98:102));
outputT_cor= ...
       [       0.697     0.723    0.749    0.776     0.804
   0.668     0.692    0.717    0.743     0.769
   0.643     0.666    0.690    0.714     0.739
   0.622     0.644    0.666    0.690     0.713
   0.606     0.627    0.648    0.670     0.693];
tol1=2e-3;
assert( max(max(abs(outputT-outputT_cor) ) ) < tol1 , ...
    [ '\nYour output [y] = [\n' sprintf('%8.3f  %8.3f %8.3f %8.3f  %8.3f \n',outputT') ']\n' ...
      '\nExpected output [y] = [\n' sprintf('%8.3f  %8.3f %8.3f %8.3f  %8.3f \n',outputT_cor') ']\n\n' ], ...
     outputT,outputT_cor);
 %%
disp('plateTemp1Test3')
nLoc=6;
hs=zeros(nLoc,3);
rng(1245)
hs(:,1)=randi(nLoc,nLoc,1);
hs(:,2)=6*rand(nLoc,1)-3;
hs(:,3)=6*rand(nLoc,1)-3;
n=200;
plotsOn='on';
[TVals]=plateTemp1(n,tPlate3,hs,plotsOn);
outputT=(TVals(98:102,98:102));
outputT_cor= ...
       [      1.686     1.795    1.951    2.155     2.403
   1.636     1.749    1.909    2.115     2.364
   1.584     1.702    1.865    2.072     2.321
   1.532     1.653    1.818    2.026     2.275
   1.480     1.603    1.770    1.977     2.225];
tol1=2e-3;
assert( max(max(abs(outputT-outputT_cor) ) ) < tol1 , ...
    [ '\nYour output [y] = [\n' sprintf('%8.3f  %8.3f %8.3f %8.3f  %8.3f \n',outputT') ']\n' ...
      '\nExpected output [y] = [\n' sprintf('%8.3f  %8.3f %8.3f %8.3f  %8.3f \n',outputT_cor') ']\n\n' ], ...
     outputT,outputT_cor);
 %%
disp('Performance Timing Test')
nLoc=10;
hs=zeros(nLoc,3);
rng(1234)
hs(:,1)=randi(nLoc,nLoc,1);
hs(:,2)=6*rand(nLoc,1)-3;
hs(:,3)=6*rand(nLoc,1)-3;
n=3000;
plotsOn='off';

tic;
[TVals]=plateTemp1(n,tPlate2,hs,plotsOn);
time_student=toc

tic;
[TVals]=plateTemp1Ref(n,tPlate2,hs,plotsOn);
time_solution=toc

assert( (time_student/time_solution) < 1.2 , ...
    [ '\nYour timing \n P = [' sprintf(' %8.4f ',time_student) ']\n'   ...
      'Solution timing \n P = [' sprintf(' %8.4f ',time_solution) ']\n'   ], ...
      time_student,time_solution);