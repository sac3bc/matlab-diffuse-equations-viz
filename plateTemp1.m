function [TVals]=plateTemp1(n,diffuseFunc,hs,plotsOn)
%plateTEmp1(n, diffuseFunc, hs, plotsOn): linear combination of a function
%with heat sources for temperature map
   %INPUTS: 
        %n: size of array/grid
        %diffuseFunc: the function from diffusefunctions.m
        %hs: 3 column array, #rows = heat source locations, column 1 = a,
        %column 2 = x location of heat sources column 3 = y locations of
        %heat sources
        %plotsOn: if 'on', plots a contour map with heat source locations
%OUTPUT:
    %TVals: n by n matrix made from linear combinations of diffuse function
%preallocate TVals
TVals = zeros(n, n); 
%get a from column 1, u from columns 2- 3
a = hs(:,1);
u = hs(:, 2: 3);
%kMax = # rows
[kMax, c] = size(hs); 
%set x and y to be between -3, 3
x = linspace(-3, 3, n); 
y = linspace(-3, 3, n); 
%transpose y to be horizontal 
y= transpose(y); 
%use repmat to generate X and Y
X = repmat(x, n, 1); 
Y = repmat(y, 1, n);


 %use the k for loop to add to TVALs with a summation , using diffuseFunc
for k=1: kMax
    TVals = TVals + (a(k)  * diffuseFunc(X, Y, u(k,:)));  
   
    
end

%if plots on is on, create a contour map 
if strcmp(plotsOn, 'on')

fh = diffuseFunc; 
w = func2str(fh);


funcy1=contour(x, y, TVals, 20);

hold on 

funcy2=plot(u(:,1), u(:,2), '*'); %plot the x and y heat source locations


hold off

title(sprintf('Contour of Temperature Grid for %d heat source locations\n Using %s Diffusion Equation', kMax, w));   

legend([funcy2],{'heat source locations'});

xlabel('x'); 
ylabel('y');

end
end 
